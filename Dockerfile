FROM openjdk:11
MAINTAINER Guilherme Natividade
COPY target/api-personagens-0.0.1-SNAPSHOT.jar /var/www/api-app.jar
WORKDIR /var/www
ENTRYPOINT ["java", "-jar", "api-app.jar"]