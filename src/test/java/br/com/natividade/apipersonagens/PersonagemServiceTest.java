package br.com.natividade.apipersonagens;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.natividade.apipersonagens.model.Personagem;
import br.com.natividade.apipersonagens.service.PersonagemService;

@SpringBootTest
public class PersonagemServiceTest {

	@Autowired
	private PersonagemService service;

	@BeforeEach
	public void setUp() {
		service.limparPersonagens();
	}

	@Test
	public void deveCadastrarPersonagem() {
		// cenário

		Personagem personagem = new Personagem("Goku", 1.80);

		// ação
		service.cadastrar(personagem);

		// validação
		List<Personagem> personagens = service.getPersonagens();

		assertEquals(1, personagens.size());
		assertEquals(personagem.getNome(), personagens.get(0).getNome());
		assertEquals(personagem.getTamanho(), personagens.get(0).getTamanho());
	}

	@Test
	public void deveExcluir() {
		// cenário
		Personagem personagem = new Personagem("Vegeta", 1.25);

		service.cadastrar(personagem);

		// ação
		service.deletar(personagem.getNome());

		List<Personagem> personagens = service.getPersonagens();

		// validação
		assertEquals(0, personagens.size());

	}

	@Test
	public void deveObterErroAoGravarPersonagemMesmoNome() {
		// cenário
		Personagem personagem = new Personagem("Vegeta", 1.25);
		Personagem personagem2 = new Personagem("Goku", 1.20);
		Personagem personagem3= new Personagem("VEGETA", 1.75);
		
		// ação
		service.cadastrar(personagem);
		service.cadastrar(personagem2);
		
		// validação
		RuntimeException error = Assertions.assertThrows(RuntimeException.class, () -> {
			service.cadastrar(personagem3);
		});
		
		assertEquals("Personagem já cadastrado", error.getMessage());
	}
}
