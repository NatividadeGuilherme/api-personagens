package br.com.natividade.apipersonagens.model;

public class Poder {
	private int numero;
	private String nome;
	public int getNumero() {
		return numero;
	}
	public String getNome() {
		return nome;
	}
	public Poder(int numero, String nome) {
		this.numero = numero;
		this.nome = nome;
	}
	
	
}
