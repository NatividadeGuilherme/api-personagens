package br.com.natividade.apipersonagens.model;

import java.util.ArrayList;
import java.util.List;

public class Personagem {
	private String nome;
	private double tamanho;
	private List<Poder> poderes = new ArrayList<>();
	
	private String teste;
	
	public String getNome() {
		return nome;
	}

	public double getTamanho() {
		return tamanho;
	}

	public Personagem(String nome, double tamanho) {
		this.nome = nome;
		this.tamanho = tamanho;
	}

	public void adicionaPoder(Poder poder) {
		this.poderes.add(poder);
	}
	
	public List<Poder> getPoderes() {
		return poderes;
	}
}
