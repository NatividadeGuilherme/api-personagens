package br.com.natividade.apipersonagens.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.natividade.apipersonagens.model.Personagem;

@Service
public class PersonagemService {

	private List<Personagem> personagens = new ArrayList<>();

	public void cadastrar(Personagem personagem) {
		if(getPersonagemNome(personagem.getNome()).isPresent())
			throw new RuntimeException("Personagem já cadastrado");
		
		personagens.add(personagem);
	}


	public List<Personagem> getPersonagens() {
		return personagens;
	}

	public void deletar(String nome) {
		Optional<Personagem> personagem = getPersonagemNome(nome);
		
		if(personagem.isPresent())
			personagens.remove(personagem.get());
		
	}
	
	public Optional<Personagem> getPersonagemNome(String nome){
		return personagens.stream()
			.filter(p -> p.getNome().toUpperCase().equals(nome.toUpperCase()))
			.findFirst();
	}
	
	public void limparPersonagens() {
		personagens.clear();
	}

}
