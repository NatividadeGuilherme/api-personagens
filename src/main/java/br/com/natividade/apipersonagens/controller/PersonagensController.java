package br.com.natividade.apipersonagens.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.natividade.apipersonagens.model.Personagem;
import br.com.natividade.apipersonagens.model.Poder;
import br.com.natividade.apipersonagens.service.PersonagemService;

@RestController
@RequestMapping("/personagem")
public class PersonagensController {

	@Autowired
	public PersonagemService service;

	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody Personagem personagem) {
		
		try {
			service.cadastrar(personagem);
			
		} catch (RuntimeException e) {
			return ResponseEntity.
					status(HttpStatus.CONFLICT)
					.body(e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.CREATED).body("");
	}

	@GetMapping("/")
	public ResponseEntity<List<Personagem>> getPersonagens() {
		return ResponseEntity.ok(service.getPersonagens());
	}
	
	@DeleteMapping("/deletar/")
	public ResponseEntity<?> deletar(@RequestParam String nome){
		
		service.deletar(nome);
		
		return ResponseEntity.status(HttpStatus.OK).body("");
	}
	
	
	@DeleteMapping("/")
	public ResponseEntity<?> deleteAll(){
		service.limparPersonagens();
		
		return ResponseEntity.ok("Personagens apagados");
	}
	
	@PostMapping("/adicionaPoder/")
	public ResponseEntity<?> adicionaPoder(@RequestParam("nome") String personagemNome, @RequestBody Poder poder){
		Optional<Personagem> personagem = service.getPersonagemNome(personagemNome);
		
		if(personagem.isEmpty())
			return  ResponseEntity.status(HttpStatus.NOT_FOUND).body("Personagem não encontrado");
		
		Personagem personagemModificado = personagem.get();
		
		personagemModificado.adicionaPoder(poder);
		
		return ResponseEntity.ok(personagemModificado);
	}
}
